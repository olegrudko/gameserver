import { GameObjectState } from "../game-server/game-object/game-object.state";
import { FigureBody } from "../game-server/collision2d/figure-body.interface";
import { Clock } from "colyseus";
import { BaseRoom } from "../game-server/rooms/base.room";
import { PlatformRoomState } from "../rooms/platform-room.state";
import { Vector3 } from "../game-server/utils/vector";

export class BlockState extends GameObjectState {
    private width = 0.5;
    private height = 0.5;

    constructor(room: BaseRoom<PlatformRoomState>, position: Vector3) {
        super(room, "block");
        this.collisionService = room.collisionService;
        this.position = position;
        this.rigidBody = this.collisionService.createRectangleBody(
            this,
            new Vector3(this.position.x - this.width / 2, this.position.y + this.height / 2),
            new Vector3(this.position.x + this.width / 2, this.position.y - this.height / 2)
        )
    }

    onCollision2D(collisions: FigureBody[]): void {
    }

    update(clock: Clock): void {
    }

}