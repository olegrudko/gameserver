export const config = {
    server: {
        port: +process.env.PORT || 8080,
    },
    room: {
        maxClients: +process.env.MAX_CLIENTS || 1,
        name: "platform",
    },
    game: {
        startTimerSeconds: 3,
        buttonsCount: 100,
        mapDistance: 100,
        distanceBetweenPlayers: -30,
    },
    update: {
        updateTimesPerSecond: 30,
    },
    security: {
        jwt: {
            secret: process.env.JWT_SECRET || "secretKey",
        }
    }
};
