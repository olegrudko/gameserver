// index.ts (game-server-side, entrypoint)
import "reflect-metadata";
import http from "http";
import { Server } from "colyseus";
import { config } from "./config";
import { getLogger } from "./game-server/utils/logger";
import { CarsRoom } from "./rooms/cars.room";
import express from "express";

const logger = getLogger("server");

const app = express();
app.get("/", (req, res) => {
    res.send("Hello world!");
});

const gameServer = new Server({
    server: http.createServer(app),
    pingInterval: 0,
});

// gameServer.simulateLatency(200);

// register your room handlers
gameServer.define(config.room.name, CarsRoom);

// make it available to receive connections
gameServer.listen(config.server.port);
logger.info(`Listening on ws://localhost:${config.server.port}`);
