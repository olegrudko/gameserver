import { RealtimeProtocol } from "../game-server/protocols/realtime.protocol";

export interface CollisionsDataProtocol {
    id: string;
}

export interface CollisionProtocol extends RealtimeProtocol {
    objectId: string;
    collisions: CollisionsDataProtocol[];
    stopX: number;
    stopY: number;
}