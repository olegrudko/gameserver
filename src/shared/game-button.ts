import { Color } from "./color.enum";

export class GameButton {
    id: number;
    color: Color;
    order: number; // first, second e.t.c button
}