export enum Direction {
    left = "left",
    right = "right",
    none = "none"
}