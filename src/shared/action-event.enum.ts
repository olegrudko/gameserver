export enum ActionEvent {
    spawnGameObject = "spawn-game-object",
    playerInitial = "player-initial",
    collision = "collision",
    pressControllerButton = "press-controller-button",
    move = "move",
    win = "win",
}