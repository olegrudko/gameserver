import { Update } from "./update.interface";

export interface Updates {
    [key: string]: Update
}