import { Clock } from "colyseus";

export type Update = (clock: Clock) => void;