import { Clock, Delayed } from "colyseus";
import { config } from "../../config";
import { ClockUpdate } from "./clock-update";
import { Updates } from "./updates.interface";

export class ClockService {
    private updates: Updates;
    private updatesId: { [key: number]: string };
    private _clock: Clock;
    private updateDelay: Delayed;

    constructor(clock: Clock) {
        this.updates = {};
        this.updatesId = {};
        this.clock = clock;
    }

    get clock(): Clock {
        return this._clock;
    }

    set clock(clock: Clock) {
        this._clock = clock;
        this.restartUpdates();
    }

    /**
     * @param callback Pass update method that will called each game update
     * @param priority Define priority of calling this method then more then late this method will be call relative other updates
     */
    public registerForUpdates(callback: (clock: Clock) => void, priority: number = 0): ClockUpdate {
        const clockUpdate = new ClockUpdate(this);
        this.updates[`${priority}__${clockUpdate.id}`] = callback;
        this.updatesId[clockUpdate.id] = `${priority}__${clockUpdate.id}`;
        return clockUpdate;
    }

    public disposeUpdate(id: number) {
        const updateId = this.updatesId[id];
        delete this.updates[updateId];
    }

    private startUpdates() {
        this.updateDelay = this.clock.setInterval(
            () => {
                this.update(this.updates);
            },
            1000 / config.update.updateTimesPerSecond,
            [this.updates]
        );
    }

    private clearUpdates() {
        if (this.updateDelay) {
            this.updateDelay.clear();
        }
    }

    private restartUpdates() {
        this.clearUpdates();
        this.startUpdates();
    }

    private update(updates: Updates) {
        for (let update of Object.values(updates)) {
            update(this.clock);
        }
    }
}
