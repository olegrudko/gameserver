import { ClockService } from "./clock.service";

export class ClockUpdate {
    static incrementor: number = 0;
    public readonly id;
    private clockService: ClockService;

    constructor(clockService: ClockService) {
        this.id = ClockUpdate.incrementor++;
        this.clockService = clockService;
    }

    public despose() {
        this.clockService.disposeUpdate(this.id);
    }
}