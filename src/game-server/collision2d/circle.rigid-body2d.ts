import { Circle } from "detect-collisions";
import { FigureBody } from "./figure-body.interface";

export class CircleRigidBody2D extends Circle implements FigureBody {}
