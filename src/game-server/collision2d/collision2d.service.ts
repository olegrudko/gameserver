import { Clock } from "colyseus";
import { Collisions, Result } from "detect-collisions";
import { getLogger } from "../utils/logger";
import { Vector3 } from "../utils/vector";
import { ClockService } from "../clock/clock.service";
import { RectangleRigidBody2D } from "./rectangle.rigid-body2d";
import { GameObjectState } from "../game-object/game-object.state";
import { FigureBody } from "./figure-body.interface";
import { CircleRigidBody2D } from "./circle.rigid-body2d";

export class Collision2DService {
    public system: Collisions;
    private logger;
    private clockService: ClockService;


    constructor(clockService: ClockService) {
        this.clockService = clockService;
        this.logger = getLogger("platform-room");
        this.system = new Collisions();
        this.prepare();
    }

    private async prepare() {
        this.clockService.registerForUpdates(this.update.bind(this), 1000);
    }

    protected update(clock: Clock) {
        this.system.update();
    }

    public createCircleBody(
        gameObject: GameObjectState,
        position: Vector3,
        radius: number
    ): CircleRigidBody2D {
        const circle = this.system.createCircle(position.x, position.y, radius);
        this.addGameDataToBody(gameObject, circle);
        return circle;
    }

    public createRectangleBody(
        gameObject: GameObjectState,
        leftTopPoint: Vector3,
        rightBottomPoint: Vector3
    ): RectangleRigidBody2D {
        const halfWidth = Math.abs(leftTopPoint.x - rightBottomPoint.x) / 2;
        const halfHeight = Math.abs(leftTopPoint.y - rightBottomPoint.y) / 2;
        const polygon = this.system.createPolygon(
            leftTopPoint.x + halfWidth,
            leftTopPoint.y + halfHeight,
            [
                [-halfWidth, halfHeight],
                [-halfWidth, -halfHeight],
                [halfWidth, -halfHeight],
                [halfWidth, halfHeight]
            ]
        );

        this.addGameDataToBody(gameObject, polygon);
        return polygon;
    }

    // read more about result in docs of detect-collision library
    public createResult(): Result {
        return this.system.createResult();
    }

    private addGameDataToBody(gameObject: GameObjectState, figureBody: FigureBody): FigureBody {
        figureBody.gameObject = gameObject;
        return figureBody;
    }
}