import { Body } from "detect-collisions";
import { GameObjectState } from "../game-object/game-object.state";

export interface FigureBody extends Body {
    gameObject?: GameObjectState;
}
