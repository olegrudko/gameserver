export class Vector3 {
    protected _x: number;
    protected _y: number;
    protected _z: number;

    get x(): number {
        return this._x;
    }

    get y(): number {
        return this._y;
    }

    get z(): number {
        return this._z;
    }

    set x(value: number) {
        this._x = value;
    }

    set y(value: number) {
        this._y = value;
    }

    set z(value: number) {
        this._z = value;
    }

    constructor();
    /**
     * @param x Defines x component of current vector
     * @param y Defines y component of current vector
     */
    constructor(x: number, y: number);
    /**
     * @param x Defines x component of current vector
     * @param y Defines y component of current vector
     * @param z Defines z component of current vector
     */
    constructor(x: number, y: number, z: number);
    /**
     * @param vector An vector. Every components of current vector will be initialized from this parameter
     */
    constructor(vector: Vector3);
    constructor(
        xOrVector: (number | Vector3) = 0,
        y: number = 0,
        z: number = 0
    ) {
        if (xOrVector instanceof Vector3) {
            this._x = xOrVector.x;
            this._y = xOrVector.y;
            this._z = xOrVector.z;
        } else {
            this._x = xOrVector;
            this._y = y;
            this._z = z;
        }
    }

    /**
     * @param {Vector3} vector Add values from the vector to all components of current vector
     */
    public add(vector: Vector3): void
    /**
     * @param {number} value Add this value to all components of current vector
     */
    public add(value: number): void
    public add(value: Vector3 | number): void {
        if (value instanceof Vector3) {
            this.x += value.x;
            this.y += value.y;
            this.z += value.z;
        } else {
            this.x += value;
            this.y += value;
            this.z += value;
        }
    }

    /**
     * @param {Vector3} vector Multiply values from the vector to all components of current vector
     */
    public mul(vector: Vector3): void
     /**
      * @param {number} value Multiply this value to all components of current vector
      */
    public mul(value: number): void
    public mul(value: Vector3 | number): void {
        if (value instanceof Vector3) {
            this.x *= value.x;
            this.y *= value.y;
            this.z *= value.z;
        } else {
            this.x *= value;
            this.y *= value;
            this.z *= value;
        }
    }

    /**
     * @param {Vector3} vector Subtract values of the vector from all components of current vector
     */
    public sub(vector: Vector3): void
     /**
      * @param {number} value Subtract this value from all components of current vector
      */
    public sub(value: number): void
    public sub(value: Vector3 | number): void {
        if (value instanceof Vector3) {
            this.x -= value.x;
            this.y -= value.y;
            this.z -= value.z;
        } else {
            this.x -= value;
            this.y -= value;
            this.z -= value;
        }
    }

    /**
     * @param {Vector3} vector Divide components of current vector on corresponding components of passed vector
     */
    public div(vector: Vector3): void
     /**
      * @param {number} value Divide all components of current vector on this value
      */
    public div(value: number): void
    public div(value: Vector3 | number): void {
        if (value instanceof Vector3) {
            this.x /= value.x;
            this.y /= value.y;
            this.z /= value.z;
        } else {
            this.x /= value;
            this.y /= value;
            this.z /= value;
        }
    }

    /**
     * Inverse all components of this vector
     */
    public inverse(): void {
        this.x *= -1;
        this.y *= -1;
        this.z *= -1;
    }

    /**
     * Return new vector with absolute values of all components
     */
    public abs(): Vector3 {
        return new Vector3(
            Math.abs(this._x), 
            Math.abs(this._y), 
            Math.abs(this._z)
        );
    }

    /**
     * Return normal of current vector
     */
    public normal(): Vector3 {
        const normal = new Vector3(this);
        const absNormal = normal.abs();
        normal.mul(absNormal);
        return normal;
    }
}
