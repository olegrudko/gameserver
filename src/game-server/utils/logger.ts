import pino, { Logger } from "pino";

export const getLogger = (name: string): Logger => pino({ name, prettyPrint: true });
