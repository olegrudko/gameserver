import { verify } from "jsonwebtoken";
import { config } from "../../config";

export class JwtService<TToken> {

    public verify(accessToken: string) {
        verify(accessToken, config.security.jwt.secret);
    }

    public parse(accessToken: string): TToken {
        return verify(accessToken, config.security.jwt.secret) as TToken;
    }
}
