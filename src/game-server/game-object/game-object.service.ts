import { GameObjectState } from "./game-object.state";
import { GameObjectProtocol } from "./game-object.protocol";

export class GameObjectService {
    private gameObjects: {[key:string]: GameObjectState} = {};

    constructor() {
    }

    register(
        gameObject: GameObjectState
    ) {
        this.gameObjects[gameObject.id] = gameObject;
    }

    denyGameObject(objectId: string) {
        if (this.gameObjects[objectId] === undefined) {
            return;
        }
        delete this.gameObjects[objectId];
    }

    getAll(): GameObjectState[] {
        return Object.values(this.gameObjects);
    }

    getAllInProtocolFormat(): GameObjectProtocol[] {
        return this.getAll().map(
            gameObject => gameObject.getProtocolData()
        );
    }

    get(objectId: string): GameObjectState {
        return this.gameObjects[objectId];
    }
}