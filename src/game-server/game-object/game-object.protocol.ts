import { RealtimeProtocol } from "../protocols/realtime.protocol";

export interface GameObjectProtocol extends RealtimeProtocol{
    id: string;
    type: string;
    x: number;
    y: number;
    z: number;
}