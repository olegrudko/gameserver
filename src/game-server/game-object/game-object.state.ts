import { Schema, type } from "@colyseus/schema";
import { Clock, Delayed } from "colyseus";
import { ClockUpdate } from "../clock/clock-update";
import { ClockService } from "../clock/clock.service";
import { Vector3 } from "../utils/vector";
import { FigureBody } from "../collision2d/figure-body.interface";
import { Guid } from "guid-typescript";
import { GameObjectService } from "./game-object.service";
import { BaseRoom } from "../rooms/base.room";
import { BaseRoomState } from "../rooms/base-room.state";
import { Collision2DService } from "../collision2d/collision2d.service";
import { GameObjectProtocol } from "./game-object.protocol";

export abstract class GameObjectState extends Schema {
    @type("string")
    public readonly id: string = Guid.create().toString();
    public readonly type: string;

    private _position: Vector3;
    private updateDelay: Delayed;
    private startUpdater: ClockUpdate;
    private finishUpdater: ClockUpdate;

    protected gameObjectService: GameObjectService;
    protected collisionService: Collision2DService;
    protected clockService: ClockService;

    protected rigidBody: FigureBody;

    public constructor(protected room: BaseRoom<BaseRoomState>,
                       type: string,
                       id?: string,
                       rigidBody?: FigureBody
    ) {
        super();
        this.type = type;
        this.id = id || this.id;
        this.rigidBody = rigidBody;
        this._position = new Vector3();
        this.gameObjectService = room.gameObjectService;
        this.clockService = room.clockService;
        this.collisionService = room.collisionService;

        this.registerForUpdates();
    }

    private registerForUpdates() {
        setTimeout(() => {
            this.start();
            this.startUpdater = this.clockService.registerForUpdates(
                this.startUpdate.bind(this)
            );
            this.finishUpdater = this.clockService.registerForUpdates(
                this.finishUpdate.bind(this),
                1001 //update after collision updated. collision update priority is 1000
            );
        });
    }

    protected start(): void {}

    private startUpdate(clock: Clock) {
        this.update(clock);
        if (this.rigidBody)
        {
            this.rigidBody.x = this._position.x;
            this.rigidBody.y = this._position.y;
        }
    }

    private finishUpdate(clock: Clock) {
        if (this.rigidBody)
        {
            const potentials = this.rigidBody.potentials();
            if (potentials.length > 0 ) {
                this.onCollision2D(potentials);
            }
        }
    }

    abstract update(clock: Clock): void;

    abstract onCollision2D(collisions: FigureBody[]): void;

    get position(): Vector3 {
        return this._position;
    }

    set position(position: Vector3) {
        this._position = position;
        if (this.rigidBody) {
            this.rigidBody.x = position.x;
            this.rigidBody.y = position.y;
        }
    }

    public translate(vector: Vector3) {
        this.position.add(vector);
    }

    public getProtocolData(): GameObjectProtocol {
        return {
            id: this.id,
            type: this.type,
            x: this.position.x,
            y: this.position.y,
            z: this.position.z,
            actionTime: this.clockService.clock.elapsedTime
        }
    }

    onRemove() {
        this.gameObjectService.denyGameObject(this.id);
        this.updateDelay.clear();
        this.startUpdater.despose();
        this.finishUpdater.despose();
    }
}
