import { Schema, type } from "@colyseus/schema";

export class BaseRoomState extends Schema {
    //State cannot be empty
    @type("string")
    public name: string = "some_room";

    constructor() {
        super();
    }
}
