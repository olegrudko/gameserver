// rooms/MyRoom.ts (game-server-side, room file)
import { Room } from "colyseus";
import { getLogger } from "../utils/logger";
import { ClockService } from "../clock/clock.service";
import { GameObjectService } from "../game-object/game-object.service";
import { BaseRoomState } from "./base-room.state";
import { config } from "../../config";
import { Collision2DService } from "../collision2d/collision2d.service";

const logger = getLogger("base-room");

export class BaseRoom<T extends BaseRoomState> extends Room<T> {
    public readonly gameObjectService: GameObjectService;
    public readonly maxClients = config.room.maxClients;
    public readonly clockService: ClockService;
    public readonly collisionService: Collision2DService;

    constructor() {
        super();
        this.gameObjectService = new GameObjectService();
        this.clockService = new ClockService(this.clock);
        this.collisionService = new Collision2DService(this.clockService);
    }

    onCreate(options: any): void | Promise<any> {}
}
