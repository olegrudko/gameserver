import { PlayerState } from "../player/player.state";
import { CarsRoom } from "./cars.room";
import { ClockService } from "../game-server/clock/clock.service";
import { ClockUpdate } from "../game-server/clock/clock-update";
import { Clock } from "colyseus";
import { config } from "../config";
import { CarsRoomEventController } from "./cars-room.event-controller";

export class WinConditionService {
    private clockService: ClockService;
    private updater: ClockUpdate;
    private eventController: CarsRoomEventController;

    constructor(
        private room: CarsRoom
    ) {
        this.clockService = room.clockService;
        this.eventController = room.eventController;
        this.updater = this.clockService.registerForUpdates(
            this.update.bind(this),
            2
        );
    }

    update(clock: Clock) {
        for (let player of this.players) {
            if (!player.hasWon() && player.distance >= config.game.mapDistance) {
                player.win();
                this.eventController.winEvent(player);
                break;
            }
        }
    }

    onRemove() {
        this.updater.despose();
    }

    private get players(): PlayerState[] {
        return this.room.players;
    }
}
