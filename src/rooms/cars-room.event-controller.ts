import { PrepareStartGameProtocol } from "./prepare-start-game.protocol";
import { ClockService } from "../game-server/clock/clock.service";
import { getLogger } from "../game-server/utils/logger";
import { GameObjectProtocol } from "../game-server/game-object/game-object.protocol";
import { GameObjectState } from "../game-server/game-object/game-object.state";
import {
    CollisionProtocol,
    CollisionsDataProtocol
} from "../shared/collision.protocol";
import { ActionEvent } from "../shared/action-event.enum";
import { FigureBody } from "../game-server/collision2d/figure-body.interface";
import { GameButton } from "../shared/game-button";
import { CarsRoom } from "./cars.room";
import { PlayerState } from "../player/player.state";
import { MoveProtocol } from "../player/move.protocol";
import { config } from "../config";
import { WinProtocol } from "../player/win.protocol";
import { RatingClientService } from "@unsav/management-client/dist";
import { ApplyRatingRequest } from "@unsav/shared/dist";

const logger = getLogger("cars-room-event-controller");

export class CarsRoomEventController {
    private readonly clockService: ClockService;
    private readonly ratingClientService: RatingClientService;
    private winnerCounter = 0;

    constructor(private room: CarsRoom) {
        this.clockService = room.clockService;
        this.ratingClientService = new RatingClientService();
    }

    private get actionTime() {
        return this.clockService.clock.elapsedTime;
    }

    prepareStartGameEvent(gameButtons: GameButton[]) {
        logger.info(`prepare start game`);
        const prepareStartData: PrepareStartGameProtocol = {
            seconds: config.game.startTimerSeconds,
            mapDistance: config.game.mapDistance,
            actionTime: this.actionTime,
            gameButtons: gameButtons
        };
        this.room.broadcast("prepare-start-game", prepareStartData);
    }

    createGameObjectEvent(gameObject: GameObjectState, options?: any) {
        const data: GameObjectProtocol = gameObject.getProtocolData();
        this.room.broadcast(ActionEvent.spawnGameObject, data, options);
    }

    moveEvent(player: PlayerState) {
        const data: MoveProtocol = {
            actionTime: this.actionTime,
            sessionId: player.id,
            speed: player.speed,
            distance: player.distance
        };

        this.room.broadcast(ActionEvent.move, data);
    }

    emitCollisionEvent(collided: GameObjectState, collisions: FigureBody[]) {
        const collisionsData: CollisionsDataProtocol[] = collisions.map(
            (collision) => {
                return {
                    id: collision.gameObject.id
                };
            }
        );
        const collision: CollisionProtocol = {
            objectId: collided.id,
            collisions: collisionsData,
            stopX: collided.position.x,
            stopY: collided.position.y,
            actionTime: this.clockService.clock.elapsedTime
        };
        this.room.broadcast(ActionEvent.collision, collision);
    }

    async winEvent(player: PlayerState) {
        this.winnerCounter++;
        const winPlace = this.winnerCounter;

        // apply rating to mapi
        const applyRatingRequest: ApplyRatingRequest = {
            averageRating: this.room.averagePlayersRating,
            playerId: player.playerId,
            position: winPlace
        };
        try {
            await this.ratingClientService.applyRating(applyRatingRequest);
        } catch (error){
            logger.error(`Rating cannot be applied to the player with id ${player.playerId}`, error);
            throw error;
        }

        // confirm win place to the game client
        const data: WinProtocol = {
            sessionId: player.id,
            winPlace,
            actionTime: this.clockService.clock.elapsedTime
        };
        this.room.broadcast(ActionEvent.win, data);
    }
}
