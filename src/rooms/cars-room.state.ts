import { getLogger } from "../game-server/utils/logger";
import { BaseRoom } from "../game-server/rooms/base.room";
import { StaticRoomState } from "../game-server/rooms/static-room.state";
import { GameButton } from "../shared/game-button";
import { Color } from "../shared/color.enum";
import { config } from "../config";

const logger = getLogger("cars-room-state");


export class CarsRoomState extends StaticRoomState {
    private room: BaseRoom<CarsRoomState>;
    private _buttons: GameButton[] = [];

    constructor(room: BaseRoom<CarsRoomState>) {
        super();
        this.room = room;
        this.fillButtons();
    }

    public get buttons() {
        return this._buttons;
    }

    private fillButtons() {
        for(let i = 0; i < config.game.buttonsCount; i++) {
            this._buttons[i] = this.generateButton(i);
        }
    }

    private generateButton(id: number): GameButton {
        return {
            id: id,
            color: Color.Red,
            order: Math.floor(Math.random() * 2) // 0 or 1
        }
    }
}
