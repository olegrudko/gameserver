// rooms/MyRoom.ts (game-server-side, room file)
import { Client } from "colyseus";
import { getLogger } from "../game-server/utils/logger";
import { BaseRoom } from "../game-server/rooms/base.room";
import { PlayerState } from "../player/player.state";
import { Vector3 } from "../game-server/utils/vector";
import { config } from "../config";
import { CarsRoomEventController } from "./cars-room.event-controller";
import { CarsRoomState } from "./cars-room.state";
import { ActionEvent } from "../shared/action-event.enum";
import { PressControllerButtonProtocol } from "./press-controller-button.protocol";
import { GameButton } from "../shared/game-button";
import { WinConditionService } from "./win-condition.service";
import { ProfileService } from "@unsav/management-client";
import { JwtService } from "../game-server/utils/jwt.service";
import { TokenData } from "@unsav/shared";

const logger = getLogger("cars-room");

export class CarsRoom extends BaseRoom<CarsRoomState> {
    public readonly eventController: CarsRoomEventController;
    public readonly players: PlayerState[] = [];
    private startPosition = new Vector3(0, 0, 0);
    private distanceBetweenPlayers = config.game.distanceBetweenPlayers || 4;
    private winConditionService: WinConditionService;
    private profileService: ProfileService;
    private jwtService: JwtService<TokenData> = new JwtService<TokenData>();

    constructor() {
        super();
        this.eventController = new CarsRoomEventController(this);
        this.winConditionService = new WinConditionService(this);
        this.profileService = new ProfileService();
    }

    onCreate(options: any): void | Promise<any> {
        this.setState(new CarsRoomState(this));
        this.onMessage(
            ActionEvent.pressControllerButton,
            (client, data: PressControllerButtonProtocol) => {
                logger.info(
                    `Message from ${client.sessionId}. PressControllerButtonProtocol:  ${data.order}`
                );

                const player: PlayerState = this.getPlayer(client.sessionId);
                player.processButton(data.order);
                player.expectedButton = this.getNextButton(player);
                this.eventController.moveEvent(player);
            }
        );

        logger.info("Room has been created");
        return super.onCreate(options);
    }

    // client joined: bring your own logic
    async onJoin(client: Client, options?: any, auth?: any): Promise<any> {
        const accessToken = options?.accessToken;
        if (!accessToken) {
            throw new Error("Join error: accessToken is not defined");
        }
        this.verifyToken(accessToken);
        await this.initializeNewPlayer(client, accessToken);
        logger.info(`Client joined: ${client.sessionId}`);
        if (this.players.length === config.room.maxClients) {
            return this.prepareStartGame();
        }
    }

    // client left: bring your own logic
    onLeave(client: Client, consented?: boolean): void | Promise<any> {
        this.gameObjectService.denyGameObject(client.sessionId);
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].id != client.sessionId) {
                delete this.players[i];
                break;
            }
        }
        logger.info(`Client left: ${client.sessionId}`);
    }

    // room has been disposed: bring your own logic
    async onDispose() {
        this.winConditionService.onRemove();
    }

    public getPlayer(id: string): PlayerState {
        return <PlayerState>this.gameObjectService.get(id);
    }

    private generateNewPlayerPosition(): Vector3 {
        return new Vector3(
            this.startPosition.x,
            this.startPosition.y,
            this.startPosition.z +
                this.distanceBetweenPlayers * (this.players.length + 1)
        );
    }

    private async initializeNewPlayer(client: Client, accessToken: string): Promise<PlayerState> {
        const startPosition = this.generateNewPlayerPosition();
        const tokenData: TokenData = this.jwtService.parse(accessToken);
        let profile;
        try {
            // Use it as timed example
            profile = await this.profileService.findOne(tokenData.userId.toString());
        } catch (error) {
            logger.warn("Profile is not found");
            throw error;
        }
        const player = new PlayerState(
            this,
            client,
            startPosition,
            profile,
            accessToken
        );
        this.players.push(player);
        player.sendInitialEvent();
        this.gameObjectService.register(player);
        this.eventController.createGameObjectEvent(player);
        return player;
    }

    private async prepareStartGame() {
        await this.lock();
        this.eventController.prepareStartGameEvent(this.state.buttons);
        for (let player of this.players) {
            player.expectedButton = this.state.buttons[0];
        }
    }

    private getNextButton(player: PlayerState): GameButton {
        const currentId = player.expectedButton.id;
        if (currentId < this.state.buttons.length - 1) {
            return this.state.buttons[currentId + 1];
        }
        // get at start for looping
        return this.state.buttons[0];
    }

    private verifyToken(accessToken: string) {
        try {
            this.jwtService.verify(accessToken);
        } catch {
            throw new Error("Player can't join: Access token is invalid");
        }
    }

    public get averagePlayersRating(): number {
        let ratingsSum = 0;
        for(const player of this.players) {
            ratingsSum += player.rating;
        }
        return (ratingsSum / this.players.length);
    }
}
