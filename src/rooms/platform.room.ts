// // rooms/MyRoom.ts (game-server-side, room file)
// import { Client } from "colyseus";
// import { PlatformRoomState } from "./platform-room.state";
// import { getLogger } from "../game-server/utils/logger";
// import { BaseRoom } from "../game-server/rooms/base.room";
// import { InitialProtocol } from "../player/initial.protocol";
// import { ActionEvent } from "../shared/action-event.enum";
// import { MoveProtocol } from "../player/move.protocol";
// import { PlayerState } from "../player/player.state";
// import { Vector3 } from "../game-server/utils/vector";
//
// const logger = getLogger("platform-room");
//
// export enum PlatformRoomEvent {
//     move = "move"
// }
//
// export class PlatformRoom extends BaseRoom<PlatformRoomState> {
//     // number of clients per room
//     // (colyseus will create the room instances for you)
//
//
//     constructor() {
//         super();
//     }
//
//     onCreate(options: any): void | Promise<any> {
//         this.setState(new PlatformRoomState(this));
//
//         this.onMessage(
//             PlatformRoomEvent.move,
//             (client, data: MoveProtocol) => {
//                 logger.info(
//                     `Message from ${client.sessionId} ${data.direction}`
//                 );
//                 const player: PlayerState =
//                     <PlayerState>this.gameObjectService.get(client.sessionId);
//                 player.startMove(data);
//             }
//         );
//
//         logger.info("Room has been created");
//         return super.onCreate(options);
//     }
//
//     // client joined: bring your own logic
//     onJoin(client: Client, options?: any, auth?: any): void | Promise<any> {
//         const player = new PlayerState(this, client.sessionId, new Vector3());
//         this.gameObjectService.register(
//             player,
//             { except: client }
//         );
//         const gameObjects = this.gameObjectService.getAllInProtocolFormat();
//         const initialData: InitialProtocol = {
//             sessionId: client.sessionId,
//             actionTime: this.clockService.clock.elapsedTime,
//             gameObjects: gameObjects
//         };
//         client.send(ActionEvent.playerInitial, initialData);
//         logger.info(`Client joined: ${client.sessionId}`);
//     }
//
//     // client left: bring your own logic
//     onLeave(client: Client, consented?: boolean): void | Promise<any> {
//         this.gameObjectService.denyGameObject(client.sessionId);
//         logger.info(`Client left: ${client.sessionId}`);
//     }
//
//     // room has been disposed: bring your own logic
//     async onDispose() {
//     }
// }
