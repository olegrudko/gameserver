import { getLogger } from "../game-server/utils/logger";
import { BaseRoom } from "../game-server/rooms/base.room";
import { StaticRoomState } from "../game-server/rooms/static-room.state";

const logger = getLogger("platform-state");


export class PlatformRoomState extends StaticRoomState {
    private room: BaseRoom<PlatformRoomState>;

    constructor(room: BaseRoom<PlatformRoomState>) {
        super();
        this.room = room;
    }

    // private Vector2 maxRightPosition = new Vector2(4.75f, 0);
    // private Vector2 maxLeftPosition = new Vector2(-4.75f, 0);
    // private maxRightPosition = 4.75;
    // private maxLeftPosition = -4.75;
}
