import { RealtimeProtocol } from "../game-server/protocols/realtime.protocol";
import { GameButton } from "../shared/game-button";

export interface PrepareStartGameProtocol extends RealtimeProtocol {
    seconds: number;
    mapDistance: number;
    gameButtons: GameButton[]
}