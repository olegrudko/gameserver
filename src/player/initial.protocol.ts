import { RealtimeProtocol } from "../game-server/protocols/realtime.protocol";
import { GameObjectProtocol } from "../game-server/game-object/game-object.protocol";

export interface InitialProtocol extends RealtimeProtocol {
    id: string;
    gameObjects: GameObjectProtocol[];
}