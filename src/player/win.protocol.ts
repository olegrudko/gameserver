import { RealtimeProtocol } from "../game-server/protocols/realtime.protocol";

export interface WinProtocol extends RealtimeProtocol {
    sessionId: string;
    winPlace: number;
}