import { RealtimeProtocol } from "../game-server/protocols/realtime.protocol";

export interface MoveProtocol extends RealtimeProtocol {
    sessionId: string;
    speed: number;
    distance: number;

}