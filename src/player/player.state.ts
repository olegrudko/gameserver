import { Client, Clock } from "colyseus";
import { Direction } from "../shared/direction.enum";
import { GameObjectState } from "../game-server/game-object/game-object.state";
import { getLogger } from "../game-server/utils/logger";
import { Vector3 } from "../game-server/utils/vector";
import { FigureBody } from "../game-server/collision2d/figure-body.interface";
import { ActionEvent } from "../shared/action-event.enum";
import { InitialProtocol } from "./initial.protocol";
import { CarsRoomEventController } from "../rooms/cars-room.event-controller";
import { CarsRoom } from "../rooms/cars.room";
import { GameButton } from "../shared/game-button";
import { ProfileResponse } from "@unsav/shared"

export class PlayerState extends GameObjectState {
    public speed: number = 0.5;
    public distance: number = 0;
    public direction: Direction;
    public expectedButton: GameButton;
    private width = 2;
    private height = 0.5;
    private logger;
    private client: Client;
    private won: boolean = false;
    private profile: ProfileResponse;
    public accessToken: string;

    protected eventController: CarsRoomEventController;

    constructor(
        room: CarsRoom,
        client: Client,
        position: Vector3,
        profile: ProfileResponse,
        accessToken: string
    ) {
        super(room, "player", client.id);
        this.client = client;
        this.accessToken = accessToken;
        this.profile = profile;
        this.eventController = room.eventController;
        this.position = position;
        const x1 = this.position.x - this.width / 2;
        const x2 = this.position.x + this.width / 2;
        const y1 = this.position.y + this.height / 2;
        const y2 = this.position.y - this.height / 2;
        this.rigidBody = this.collisionService.createRectangleBody(
            this,
            new Vector3(x1, y1),
            new Vector3(x2, y2)
        );
        this.logger = getLogger("platform-state");

        this.direction = Direction.none;
    }

    public update(clock: Clock): void {
        this.distance += this.speed * clock.deltaTime / 1000;
    }

    onCollision2D(collisions: FigureBody[]): void {
        // for (const collision of collisions) {
        //     const result = this.collisionService.createResult();
        //     this.rigidBody.collides(collision, result);
        //     this.position.sub(
        //         new Vector3(
        //             result.overlap * result.overlap_x,
        //             result.overlap * result.overlap_y
        //         )
        //     );
        // }
        // this.eventController.emitCollisionEvent(this, collisions);
    }

    public sendInitialEvent() {
        const gameObjects = this.gameObjectService.getAllInProtocolFormat();
        const initialData: InitialProtocol = {
            id: this.id,
            actionTime: this.clockService.clock.elapsedTime,
            gameObjects: gameObjects
        };
        this.client.send(ActionEvent.playerInitial, initialData);
    }

    public processButton(order: number): boolean {
        const match = this.expectedButton.order === order;
        if(match) {
            this.speed++;
        } else {
            if (this.speed >= 1) {
                this.speed--;
            }
        }
        this.logger.info(
            `Player ${this.id}. Button: ${order}; match:  ${match}`);
        return match;
    }

    public leave() {
        this.leave();
    }

    public win() {
        this.won = true;
    }

    public hasWon(): boolean {
        return this.won;
    }

    public get rating(): number {
        return this.profile.rating;
    }

    public get nickname(): string {
        return this.profile.nickname;
    }

    // returns player id in mapi system
    public get playerId(): number {
        return this.profile.playerId as number;
    }
}
