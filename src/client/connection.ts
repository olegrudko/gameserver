// connection.ts (client-side)
import { Client } from "colyseus.js";
import { config } from "../config";
import { getLogger } from "../game-server/utils/logger";
import { PlatformRoomState } from "../rooms/platform-room.state";
import { sleep } from "../game-server/utils/sleep";
import { PlayerState } from "../player/player.state";
import { Direction } from "../shared/direction.enum";

const logger = getLogger("client");
const client = new Client("ws://olympic-321018.appspot.com");

async function connect() {
    try {
        const room = await client.joinOrCreate<PlatformRoomState>(
            config.room.name
        );

        room.onStateChange((newState) => {
            logger.info(`New state: ${JSON.stringify(newState)}`);
        });

        room.onLeave((code) => {
            logger.info(`You've been disconnected. ${code}`);
        });
    } catch (e) {
        logger.error("Couldn't connect:", e);
    }
}

(async () => await connect())();
